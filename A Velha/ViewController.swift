//
//  ViewController.swift
//  A Velha
//
//  Created by Danilo Altheman on 14/07/15.
//  Copyright © 2015 Quaddro - Danilo Altheman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var view6: UIView!
    @IBOutlet weak var view7: UIView!
    @IBOutlet weak var view8: UIView!
    @IBOutlet weak var view9: UIView!
    
    var turn: Bool = false
    
    var row0: [String]!
    var row1: [String]!
    var row2: [String]!
    
    var clickCount: Int = 0
    
    func startGame() {
        clickCount = 0
        row0 = ["", "", ""]
        row1 = ["", "", ""]
        row2 = ["", "", ""]
        
        for view in self.view.subviews {
            if view.tag > 0 && view.tag < 10 {
                view.backgroundColor = UIColor.lightGrayColor()
                let tapGesture = UITapGestureRecognizer(target: self, action: "selectedView:")
                view.addGestureRecognizer(tapGesture)
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startGame()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func selectedView(gesture: UITapGestureRecognizer) {
        let view = gesture.view!
        
        var crossOrCircle = String()
        
        if turn {
            view.backgroundColor = UIColor.redColor()
            crossOrCircle = "x"
        }
        else {
            view.backgroundColor = UIColor.blueColor()
            crossOrCircle = "o"
        }
        
        view.removeGestureRecognizer(gesture)
        turn = !turn
        
        if view.tag >= 1 && view.tag <= 3 {
            row0[view.tag - 1] = crossOrCircle
        }
        else if view.tag >= 4 && view.tag <= 6 {
            row1[view.tag - 4] = crossOrCircle
        }
        else {
            row2[view.tag - 7] = crossOrCircle
        }
        clickCount++
        check()
    }
    
    func check() {
        print(row0)
        print(row1)
        print(row2)
        print("-------")
        let alert: UIAlertController?
        if row0[0] == "o" && row0[1] == "o" && row0[2] == "o" ||
            row1[0] == "o" && row1[1] == "o" && row1[2] == "o" ||
            row2[0] == "o" && row2[1] == "o" && row2[2] == "o" ||
            row0[0] == "o" && row1[0] == "o" && row2[0] == "o" ||
            row0[1] == "o" && row1[1] == "o" && row2[1] == "o" ||
            row0[2] == "o" && row1[2] == "o" && row2[2] == "o" ||
            row0[0] == "o" && row1[1] == "o" && row2[2] == "o" ||
            row0[2] == "o" && row1[1] == "o" && row2[0] == "o" {
                print("Jogador Azul venceu!")
                alert = UIAlertController(title: "Resultado", message: "O jogador AZUL venceu!", preferredStyle: UIAlertControllerStyle.Alert)
                alert!.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (alertAction) -> Void in
                    self.startGame()
                }))
                presentViewController(alert!, animated: true, completion: nil)
        }
        else if  row0[0] == "x" && row0[1] == "x" && row0[2] == "x" ||
            row1[0] == "x" && row1[1] == "x" && row1[2] == "x" ||
            row2[0] == "x" && row2[1] == "x" && row2[2] == "x" ||
            row0[0] == "x" && row1[0] == "x" && row2[0] == "x" ||
            row0[1] == "x" && row1[1] == "x" && row2[1] == "x" ||
            row0[2] == "x" && row1[2] == "x" && row2[2] == "x" ||
            row0[0] == "x" && row1[1] == "x" && row2[2] == "x" ||
            row0[2] == "x" && row1[1] == "x" && row2[0] == "x" {
                print("Jogador Vermelho venceu!")
                alert = UIAlertController(title: "Resultado", message: "O jogador VERMELHO venceu!", preferredStyle: UIAlertControllerStyle.Alert)
                alert!.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (alertAction) -> Void in
                    self.startGame()
                }))
                presentViewController(alert!, animated: true, completion: nil)
                
        }
        else if clickCount == 9 {
            alert = UIAlertController(title: "Resultado", message: "Deu EMPATE!", preferredStyle: UIAlertControllerStyle.Alert)
            alert!.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (alertAction) -> Void in
                self.startGame()
            }))
            presentViewController(alert!, animated: true, completion: nil)
        }
    }
}

