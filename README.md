# A Velha

Simple Tic Tac Toe game written in Swift 2.0 and iOS 9.

This source code is part of Quaddro Swift Bootcamp Training and shows how to work with:

* Arrays
* UIGestureRecognizer
* If, else if and else
* && (AND) || (OR) Operators
* UIAlertController

[www.quaddro.com.br](http://www.quaddro.com.br)